FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY . .
RUN dotnet restore ./SportCenter.Website/SportCenter.WebSite.csproj

# copy everything else and build app
COPY . ./
RUN dotnet publish ./SportCenter.Website/SportCenter.WebSite.csproj -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
ENV ASPNETCORE_URLS=http://+:5001
WORKDIR /app
EXPOSE 5001/tcp
COPY --from=build /app/out .
ENTRYPOINT ["dotnet", "SportCenter.WebSite.dll"]
