﻿using System;

namespace SportCenter.BL.Services.Member
{
    public interface IMemberService
    {
        string GetMemberInfo();
    }
}
