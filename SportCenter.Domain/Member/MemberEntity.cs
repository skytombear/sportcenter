﻿using System;

namespace SportCenter.Domain.Member
{
    public class Member
    {
        public string Id {get; set;}

        public string Name {get; set;}
    }
}
