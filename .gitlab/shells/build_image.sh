# Step: Build docker image.
GITLAB_TAG=$1

echo ">> docker build ${GITLAB_TAG}"

docker build --pull \
    --build-arg VERSION=${GITLAB_TAG} \
    --build-arg PROJECT=${PROJECT_PATH} \
    -t "${CI_REGISTRY_IMAGE}:${GITLAB_TAG}" \
    -f "$./Dockerfile" .
