# Step: Push docker image to gitlab registry.
GITLAB_TAG=$1

echo ">> docker push ${CI_REGISTRY_IMAGE}:${GITLAB_TAG}"

docker push "${CI_REGISTRY_IMAGE}:${GITLAB_TAG}"