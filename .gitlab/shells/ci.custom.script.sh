#!/usr/bin/env bash
# 此檔案可以使用所有 gitlab ci 所提供的常數: CI_COMMIT_REF_NAME(branch name)
# 可於此 script 內使用的內建變數有(請不要修改內建變數):
# - TAG_ID: docker image 的版號
# - AM_USER: gitlab variable 自設定的 private artifact manager 使用帳號
# - AM_PASSWORD: gitlab variable 自設定的 private artifact manager 使用密碼
## 以下只有 release job 可以用
# - CLOUD: 雲的類型(AWS、GCP)
# - MARKET: 市場類型(tw、my 等等)，從 git branch 發動的一率都是 tw
# - SILO_ID: 從 cluster 就被隔離的類型(px 等等)
# - ENV: 環境類型(只有 prod、qa*、stage)

init_pipeline() {
    PROJECTS=( 
        "NineYi.Service.LineBot"
    )

    source .gitlab/shells/ci.custom.script.sh
    for ((i=0; i<1; i++)); do
        PROJECT_PATH=${PROJECTS[$i]}

        echo ">> Init deploy pipeline for => ${PROJECT_PATH}"
        source ${PROJECT_PATH}/k8s/_global.env
        script_deploy_to_k8s $PROJECT_PATH $PROJECT_NAME
    done
}

script_deploy_to_k8s() {
    PROJECT_PATH=$1
    APP_NAME=$2

    cd $PROJECT_PATH
    curl http://gitlab.91app.com/guideline/k8s-skeleton/raw/master/shell/init-pipeline.sh > init.sh
    chmod u+x init.sh
    bash init.sh master
    rm -f init.sh
    source ci.utility.sh
    docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY

    init_cloud_permission "$CLOUD" "$MARKET" "$ENV" "$SILO_ID" "$CLUSTER_TYPE"
    REF_ID=$CI_COMMIT_REF_SLUG
    AM_TAG="${APP_NAME}_${REF_ID}_${CI_COMMIT_SHORT_SHA}"
    TAG_ID=$AM_TAG

    echo "ENV=${ENV}"
    echo "PIPELINE_ID=${CI_PIPELINE_ID}"
    echo "TAG_ID=$TAG_ID"
    deploy_k8s_config "${APP_NAME}.configmap.yaml" apply --record
    deploy_k8s_config "${APP_NAME}.deploy.yaml" apply --record
    cd -
}