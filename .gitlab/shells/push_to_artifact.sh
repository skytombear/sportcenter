_push_to_artifact() {
    GITLAB_TAG=$1
    AM_TAG=$2
    PROJECT_PATH=$3

    echo ">> GITLAB_TAG = ${GITLAB_TAG}"
    echo ">> AM_TAG = ${AM_TAG}"

    source "${PROJECT_PATH}/k8s/_global.env"
    echo ">> CR = ${CR}"
    echo ">> REPO = ${REPO}"

    IMAGE="${CR}/${REPO}"
    echo ">> IMAGE = ${IMAGE}"

    _check_image_on_am
    result=$?

    if [ $result -eq 0 ]; then
        echo ""
        echo ">> Image already exists on AM."
        echo ""
    else
        echo ""
        echo ">> Images not found on AM."
        echo ">> Pushing image to AM."
        echo 'docker login -u $AM_USER -p $AM_PASSWORD $CR'
        docker login -u ${AM_USER} -p ${AM_PASSWORD} ${CR}
        # 2020-11-27 修正 GitLab CI/CD 錯誤
        # 參考： https://91app.slack.com/archives/G01DGEH8D4K/p1606463482020300?thread_ts=1606451309.017300&cid=G01DGEH8D4K
        # echo 'docker pull ${CI_REGISTRY_IMAGE}:${GITLAB_TAG}'
        # docker pull ${CI_REGISTRY_IMAGE}:${GITLAB_TAG}
        # [[ $? -eq 1 ]] &&  echo ">> Image not found on gitlab. Make sure you completed the build process."
        echo 'docker tag ${CI_REGISTRY_IMAGE}:${GITLAB_TAG}  ${IMAGE}:${AM_TAG}'
        docker tag ${CI_REGISTRY_IMAGE}:${GITLAB_TAG} ${IMAGE}:${AM_TAG}
        echo 'docker push ${IMAGE}:${AM_TAG}'
        docker push ${IMAGE}:${AM_TAG}
    fi
}

# 確認 image 是否已經存在在 AM 上
_check_image_on_am() {
    echo ">> Check if ${IMAGE}:${AM_TAG} exists on AM."
    echo ">> docker pull ${IMAGE}:${AM_TAG} > /dev/null"
    docker -v
    docker login -u ${AM_USER} -p ${AM_PASSWORD} ${CR}
    docker pull ${IMAGE}:${AM_TAG} > /dev/null
    result=$?
    docker rmi ${IMAGE}:${AM_TAG}
    return $result
}

_check_image_on_gitlab() {
    echo ">> Check if  ${CI_REGISTRY_IMAGE}:${GITLAB_TAG} exists on Gitlab."
    echo ">> docker pull ${CI_REGISTRY_IMAGE}:${GITLAB_TAG} > /dev/null"
    docker pull ${CI_REGISTRY_IMAGE}:${GITLAB_TAG} > /dev/null
    result=$?
    return $result
}


_push_to_artifact $1 $2 $3