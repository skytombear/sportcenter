<!-- 請將 Merge-request title 加上 "chore:" prefix -->
<!-- 任何非 application 層的改動都適用 -->

- ticket number: `vsts_xxxx`
- 這支 PR 會動到 ...
  - [ ] 環境變數 (\*.env ...)
  - [ ] CI / CD 流程 (\*.gitlab-ci.yml ...)
  - [ ] k8s / docker 運作流程 (Dockerfile, k8s/\*_/_ ... )
  - [ ] 開發流程 (.vscode/\*, ...)
  - [ ] 文件 (\*.md)

## Summary

### Purpose

<!-- 請簡單說明這支 PR 的目標 -->
None.

### CI/CD Change

<!-- 是否有動到 ci / cd 流程 -->

None.

### Environment/Infra Change

<!-- 是否有動到環境設定、Docker/k8s 配置、開發流程等 -->

None.

### Extra Testing

<!-- 是否有需要在 merge 後特別關注、測試的項目 (可能會影響到線上的) -->

None.

### Reviewer

<!-- 是否有需要特別請誰協助 review 該 PR -->

None.

# Checklist

- [ ] 我的 code 已經有自己**驗證過功能**了