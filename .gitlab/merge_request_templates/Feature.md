<!-- 請將 Merge-request title 加上 "feat:" prefix -->
<!-- 任何 application 層的改動都適用 -->

- ticket number: `vsts_xxxx`
- 這支 PR 會動到 ...
  - [ ] 環境變數 (\*.env ...)
  - [ ] 調整 dependencies (NineYi.Service.Gaia.sln, *.csproject)
  - [ ] 新增 Migrations / SQL Script (Migrations/\*, SQL/\* ...)
  - [ ] 全域 utils
  - [ ] 檔案結構

## Summary

### Purpose

<!-- 請簡單說明這支 PR 的目標 -->
None.

### Environment/Infra Change

<!-- 是否有動到環境設定、Docker/k8s 配置、開發流程等 -->

None.

### Extra Testing

<!-- 是否有需要在 merge 後特別關注、測試的項目 (可能會影響到線上的) -->

None.

### Reviewer

<!-- 是否有需要特別請誰協助 review 該 PR -->

None.

# Checklist

- [ ] 我的 code 已經有自己**驗證過功能**了
- [ ] 我的 code 都有 follow coding style 並且會**通過 stylecop 的驗證**
- [ ] 我的 code 都有**寫測試**
